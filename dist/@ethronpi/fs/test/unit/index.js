"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

const assert = _core.dogma.use(require("@ethronjs/assert"));

const pkg = _core.dogma.use(require("../../../../@ethronpi/fs"));

module.exports = exports = (0, _ethron.test)(__filename, () => {
  {
    assert(pkg).mem("append").isCallable().mem("cp").isCallable().mem("exists").isCallable().mem("ls").isCallable().mem("mkdir").isCallable().mem("mv").isCallable().mem("rm").isCallable();
  }
});