"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

const assert = _core.dogma.use(require("@ethronjs/assert"));

const exists = _core.dogma.use(require("../../../../../@ethronpi/fs/src/fns/exists"));

module.exports = exports = (0, _ethron.suite)(__filename, () => {
  {
    (0, _ethron.suite)("exists({path})", () => {
      {
        (0, _ethron.test)("exists() : true", async () => {
          {
            assert((0, await exists({
              ["path"]: __filename
            }))).eq(true);
          }
        });
        (0, _ethron.test)("exists() : false", async () => {
          {
            assert((0, await exists({
              ["path"]: "unknown"
            }))).eq(false);
          }
        });
      }
    });
    (0, _ethron.suite)("exists({path, raise})", () => {
      {
        (0, _ethron.test)("exists() : true", async () => {
          {
            assert((0, await exists({
              ["path"]: __filename,
              ["raise"]: true
            }))).eq(true);
          }
        });
        (0, _ethron.test)("exists() : false", async () => {
          {
            assert((await _core.dogma.pawait(() => exists({
              ["path"]: "unknown",
              ["raise"]: true
            })))).eq([false, "Path not found: unknown."]);
          }
        });
      }
    });
  }
});