"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

const assert = _core.dogma.use(require("@ethronjs/assert"));

const ls = _core.dogma.use(require("../../../../../@ethronpi/fs/src/fns/ls"));

module.exports = exports = (0, _ethron.suite)(__filename, () => {
  {
    (0, _ethron.suite)("with dir found", () => {
      {
        (0, _ethron.test)("ls({path})", async () => {
          {
            const items = (0, await ls({
              ["path"]: __dirname
            }));
            assert((0, _core.len)(items)).gt(1);

            for (let item of items) {
              assert(item).like("^[a-z].+\\.js$");
            }
          }
        });
        (0, _ethron.test)("ls(ṕath, abs)", async () => {
          {
            const items = (0, await ls({
              ["path"]: __dirname,
              ["abs"]: true
            }));
            assert((0, _core.len)(items)).gt(1);

            for (let item of items) {
              assert(item).like("^[^a-z].+\\.js$");
            }
          }
        });
      }
    });
    (0, _ethron.test)("with dir not found", async params => {
      /* istanbul ignore next */
      _core.dogma.paramExpectedToHave("params", params, {
        abs: {
          type: _core.bool,
          mandatory: true
        }
      }, true);

      let {
        abs
      } = params;
      {
        assert((await _core.dogma.pawait(() => ls({
          ["path"]: "unknown",
          ["abs"]: abs
        })))).item(0).eq(false).item(1).like("no such file or directory");
      }
    }).forEach({
      ["abs"]: true
    }, {
      ["abs"]: false
    });
  }
});