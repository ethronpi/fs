"use strict";

var _core = require("@dogmalang/core");

const fs = _core.dogma.use(require("@dogmalang/fs.async"));

async function exists(params) {
  let exists = false;
  /* istanbul ignore next */

  _core.dogma.paramExpectedToHave("params", params, {
    path: {
      type: _core.text,
      mandatory: true
    },
    raise: {
      type: _core.bool,
      mandatory: false
    }
  }, true);

  let {
    path,
    raise
  } = params;
  {
    exists = (0, await fs.exists(path));

    if (raise && !exists) {
      _core.dogma.raise("Path not found: %s.", path);
    }
  }
  return exists;
}

module.exports = exports = exists;