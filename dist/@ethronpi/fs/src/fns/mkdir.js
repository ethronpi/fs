"use strict";

var _core = require("@dogmalang/core");

const fsx = _core.dogma.use(require("fs-extra"));

async function mkdir(params) {
  /* istanbul ignore next */
  _core.dogma.paramExpectedToHave("params", params, {
    path: {
      type: _core.text,
      mandatory: true
    }
  }, true);

  let {
    path
  } = params;
  {
    0, await fsx.mkdirp(path);
  }
}

module.exports = exports = mkdir;