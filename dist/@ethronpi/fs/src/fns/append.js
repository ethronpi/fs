"use strict";

var _core = require("@dogmalang/core");

const fs = _core.dogma.use(require("@dogmalang/fs.async"));

async function append(params) {
  /* istanbul ignore next */
  _core.dogma.paramExpectedToHave("params", params, {
    path: {
      type: _core.text,
      mandatory: true
    },
    data: {
      type: _core.any,
      mandatory: true
    },
    line: {
      type: _core.num,
      mandatory: false
    }
  }, true);

  let {
    path,
    data,
    line
  } = params;
  {
    const file = fs.file(path);

    if (params.line == null) {
      0, await file.append(data);
    } else {
      let ix;
      let c;
      c = (0, await file.read()).split("\n");
      ix = line < 0 ? line + (0, _core.len)(c) : line;

      if (ix < 0 || ix >= (0, _core.len)(c)) {
        _core.dogma.raise("line out of range.");
      }

      c.splice(ix, 0, data);
      0, await file.write(c.join("\n"));
    }
  }
}

module.exports = exports = append;