"use strict";

var _core = require("@dogmalang/core");

const fs = _core.dogma.use(require("@dogmalang/fs.async"));

async function ls(params) {
  /* istanbul ignore next */
  _core.dogma.paramExpectedToHave("params", params, {
    path: {
      type: _core.text,
      mandatory: true
    },
    abs: {
      type: _core.bool,
      mandatory: false
    }
  }, true);

  let {
    path,
    abs
  } = params;
  {
    return 0, await fs.dir(path).read({
      ["name"]: abs ? "full" : null
    });
  }
}

module.exports = exports = ls;