"use strict";

var _core = require("@dogmalang/core");

const fsx = _core.dogma.use(require("fs-extra"));

async function copy(params) {
  /* istanbul ignore next */
  _core.dogma.paramExpectedToHave("params", params, {
    src: {
      type: _core.text,
      mandatory: true
    },
    dst: {
      type: _core.text,
      mandatory: true
    },
    force: {
      type: _core.bool,
      mandatory: false
    }
  }, true);

  let {
    src,
    dst,
    force
  } = params;
  {
    0, await fsx.copy(src, dst, {
      ["override"]: (0, _core.bool)(force)
    });
  }
}

module.exports = exports = copy;