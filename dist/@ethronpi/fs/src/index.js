"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ls = exports.exists = exports.append = exports.mkdir = exports.mv = exports.rm = exports.cp = void 0;

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

const cp = (0, _ethron.simple)({
  ["id"]: "cp",
  ["desc"]: "Copy a file or directory",
  ["fmt"]: params => {
    /* istanbul ignore next */
    _core.dogma.paramExpected("params", params, _core.map);

    let {
      src,
      dst
    } = params;
    {
      return `Copy '${src}' to '${dst}'`;
    }
  },
  ["fmtParams"]: params => {
    /* istanbul ignore next */
    _core.dogma.paramExpected("params", params, null);

    {
      return {
        ["src"]: _core.dogma.getItem(params, 0),
        ["dst"]: _core.dogma.getItem(params, 1)
      };
    }
  }
}, _core.dogma.use(require("./fns/cp")));
exports.cp = cp;
const rm = (0, _ethron.simple)({
  ["id"]: "rm",
  ["desc"]: "Remove a file or directory.",
  ["fmt"]: params => {
    /* istanbul ignore next */
    _core.dogma.paramExpected("params", params, _core.map);

    let {
      path
    } = params;
    {
      return `Remove '${path}'`;
    }
  },
  ["fmtParams"]: params => {
    /* istanbul ignore next */
    _core.dogma.paramExpected("params", params, null);

    {
      return {
        ["path"]: _core.dogma.getItem(params, 0)
      };
    }
  }
}, _core.dogma.use(require("./fns/rm")));
exports.rm = rm;
const mv = (0, _ethron.simple)({
  ["id"]: "mv",
  ["desc"]: "Move a file or directory.",
  ["fmt"]: params => {
    /* istanbul ignore next */
    _core.dogma.paramExpected("params", params, _core.map);

    let {
      src,
      dst
    } = params;
    {
      return `Move '${src}' to '${dst}'`;
    }
  },
  ["fmtParams"]: params => {
    /* istanbul ignore next */
    _core.dogma.paramExpected("params", params, null);

    {
      return {
        ["src"]: _core.dogma.getItem(params, 0),
        ["dst"]: _core.dogma.getItem(params, 1)
      };
    }
  }
}, _core.dogma.use(require("./fns/mv")));
exports.mv = mv;
const mkdir = (0, _ethron.simple)({
  ["id"]: "mkdir",
  ["desc"]: "Create a directory.",
  ["fmt"]: params => {
    /* istanbul ignore next */
    _core.dogma.paramExpected("params", params, _core.map);

    let {
      path
    } = params;
    {
      return `Create dir '${path}'`;
    }
  },
  ["fmtParams"]: params => {
    /* istanbul ignore next */
    _core.dogma.paramExpected("params", params, null);

    {
      return {
        ["path"]: _core.dogma.getItem(params, 0)
      };
    }
  }
}, _core.dogma.use(require("./fns/mkdir")));
exports.mkdir = mkdir;
const append = (0, _ethron.simple)({
  ["id"]: "append",
  ["desc"]: "Append content to a file.",
  ["fmt"]: params => {
    /* istanbul ignore next */
    _core.dogma.paramExpected("params", params, _core.map);

    let {
      path
    } = params;
    {
      return `Append content to '${path}'`;
    }
  },
  ["fmtParams"]: params => {
    /* istanbul ignore next */
    _core.dogma.paramExpected("params", params, null);

    {
      return {
        ["path"]: _core.dogma.getItem(params, 0),
        ["data"]: _core.dogma.getItem(params, 1)
      };
    }
  }
}, _core.dogma.use(require("./fns/append")));
exports.append = append;
const exists = (0, _ethron.simple)({
  ["id"]: "exists",
  ["desc"]: "Check whether a path exists.",
  ["fmt"]: params => {
    /* istanbul ignore next */
    _core.dogma.paramExpected("params", params, _core.map);

    let {
      path
    } = params;
    {
      return `Check whether '${path}' exists`;
    }
  },
  ["fmtParams"]: params => {
    /* istanbul ignore next */
    _core.dogma.paramExpected("params", params, null);

    {
      return {
        ["path"]: _core.dogma.getItem(params, 0)
      };
    }
  }
}, _core.dogma.use(require("./fns/exists")));
exports.exists = exists;
const ls = (0, _ethron.simple)({
  ["id"]: "ls",
  ["desc"]: "List directory entries.",
  ["fmt"]: params => {
    /* istanbul ignore next */
    _core.dogma.paramExpected("params", params, _core.map);

    let {
      path
    } = params;
    {
      return `Get '${path}' directory entries`;
    }
  },
  ["fmtParams"]: params => {
    /* istanbul ignore next */
    _core.dogma.paramExpected("params", params, null);

    {
      return {
        ["path"]: _core.dogma.getItem(params, 0),
        ["abs"]: _core.dogma.getItem(params, 1)
      };
    }
  }
}, _core.dogma.use(require("./fns/ls")));
exports.ls = ls;