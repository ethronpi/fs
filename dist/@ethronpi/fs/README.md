# @ethronpi/fs

[![NPM version](https://img.shields.io/npm/v/@ethronpi/fs.svg)](https://npmjs.org/package/@ethronpi/fs)
[![Total downloads](https://img.shields.io/npm/dt/@ethronpi/fs.svg)](https://npmjs.org/package/@ethronpi/fs)

[Ethron.js](http://ethronlabs.com) plugin for file system ops.

*Developed in [Dogma](http://dogmalang.com), compiled to JavaScript.*

*Engineered in Valencia, Spain, EU by EthronLabs.*

## Use

```
const fs = require("@ethronpi/fs");
```

## fs.cp task

It copies a file or directory:

```
fs.cp({src, dst, force})
fs.cp(src, dst)
```

- `src` (string, required). File/directory to copy.
- `dst` (string, required). Where to copy.
- `force` (bool). Must the file be copied if `dst` exists? Default: `false`.

Example:

```
fs.cp({
  src: "/opt/redis/conf/redis.conf",
  dst: "/bk/redis/conf/redis.conf",
  force: true
})

```

## fs.rm task

It removes a file or a directory:

```
fs.rm({path})
fs.rm(path)
```

- `path` (string, required). File to remove.

Example:

```
fs.rm({
  path: "/opt/redis/conf/redis.conf"
})
```

## fs.mkdir task

It creates a directory:

```
fs.mkdir({path})
fs.mkdir(path)
```

- `path` (string, required). Directory to create.

Example:

```
fs.mkdir({
  path: "/opt/redis"
})
```

## fs.mv task

It moves a file or a directory to other path:

```
fs.mv({src, dst, force})
fs.mv(src, dst)
```

- `src` (string, required). Path to move.
- `dst` (string, required). Path where to move.
- `force` (bool). Overwrite if `dst` exists? Default: `false`.

## fs.append task

It appends content to a file:

```
fs.append({path, data, line})
fs.append(path, data)
```

- `path` (string, required). File path.
- `data` (string, required). Content to add.
- `line` (number). Line where to insert the data. Negative values allowed.

Example

```
fs.append({
  src: "myfile.txt",
  data: "This is the content to append."
})
```

## fs.exists task

It checks whether a path exists:

```
fs.exists({path})
fs.exists(path)
```

- `path` (string, required). Entry path.

Example:

```
fs.exists({
  path: "myfile.txt"
})
```

# fs.ls task


It returns the directory entries:

```
fs.ls({path, abs})
fs.ls(path, abs)
```

- `path` (string, required). Directory path.
- `abs` (bool). Return the absolute child paths. Default: `false`.
