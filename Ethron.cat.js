//imports
const {cat} = require("ethron");
const exec = require("@ethronpi/exec");
const babel = require("@ethronpi/babel");
const eslint = require("@ethronpi/eslint");
const npm = require("@ethronpi/npm");

//Package name.
const pkg = require("./package").name;

//Who can publish.
const who = "ethronpi";

//catalog
cat.macro("lint", [
  [exec, "dogmac check src test"],
  [eslint, "."]
]).title("Lint source code");

cat.macro("trans-dogma", [
  [exec, "rm -rf ./build"],
  [exec, "dogmac js -o build/src src"],
  [exec, "dogmac js -o build/test test"]
]).title("Transpile from Dogma to JS");

cat.macro("trans-js", [
  [exec, "rm -rf ./dist"],
  [babel, "build", `dist/${pkg}/`]
]).title("Transpile from JS to JS");

cat.macro("build", [
  cat.get("lint"),
  cat.get("trans-dogma"),
  cat.get("trans-js"),
  [exec, `cp package.json dist/${pkg}/package.json`],
  [exec, `cp README.md dist/${pkg}/README.md`]
]).title("Build package");

cat.macro("test", `./dist/${pkg}/test/unit`).title("Unit tests");

cat.macro("dflt", [
  cat.get("build"),
  cat.get("test")
]).title("Build and test");

cat.call("pub", npm.publish, {
  who,
  path: `dist/${pkg}`,
  access: "public"
}).title("Publish on NPM");
